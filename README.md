# GitLab CI Job for .Net Api Portability Analyzer

This Working Example can be customized to analyze the compatibility level of your existing .NET codebase for refactoring for .NET Core.

The contents of [.gitlab-ci.yml](./.gitlab-ci.yml) contain helpful how to comments and links to web articles on the best way to approach a .NET Core refactoring project.

The real work is done here: [hci-cd-plugin-extension-net-portability-analyzer.yml](https://gitlab.com/guided-explorations/templates/ci-cd-plugin-extensions/ci-cd-plugin-extension-net-portability-analyzer/-/blob/master/ci-cd-plugin-extension-net-portability-analyzer.yml)

See here for documentation on what the .NET Api Portability Analyzer is and how it can help you get to .NET Core: [CI CD Plugin Extension .NET Portability Analyzer README.md](https://gitlab.com/guided-explorations/templates/ci-cd-plugin-extensions/ci-cd-plugin-extension-net-portability-analyzer/-/blob/master/README.md)
